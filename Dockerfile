# syntax=docker/dockerfile:1
FROM solr:8.11.1

# Copy our local bits into the appropriate directories
COPY . /sciveyor

# Open up our port
EXPOSE 8983

# Run our setup script
ENTRYPOINT ["solr-precreate", "sciveyor", "/sciveyor/configset"]
