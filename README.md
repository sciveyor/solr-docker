<p align="center">
<img src="https://docs.sciveyor.com/images/icon-orange.svg" width="20%"
height="auto" alt="Sciveyor Logo">
</p>

# Sciveyor Solr Docker Image

This repository contains the code for building a Docker image of Solr that can
be used with [Sciveyor.](https://www.sciveyor.com) It is also the host for the
authoritative Solr configuration and schema.

To run this image, saving data to a local folder, you can execute:

```console
$ mkdir solrdata
$ sudo chown 8983:8983 solrdata
$ sudo docker run -d -v "$PWD/solrdata:/var/solr" -p 8983:8983 --name my_solr \
  pencechp/sciveyor-solr:latest
```

You can also separate the mounting of the data store and the log and
configuration files, by running:

```console
$ sudo docker run -d \
  -v "$PWD/solrconfigs:/var/solr" \
  -v "$PWD/solrdata:/data" \
  -p 8983:8983 --name my_solr \
  -e SOLR_HOME="/data" \
  pencechp/sciveyor-solr:latest
```

After a moment, the Solr server will be available at <http://localhost:8983>.

## Environment Variables

These (and other variables,
[see this file for details](https://github.com/apache/solr/blob/main/solr/bin/solr.in.sh))
can be modified when you start the Docker container, by adding
`-e VARIABLE=value` to your `docker run` command.

- `SOLR_HEAP`: The amount of memory that Solr's JVM will use while running. Note
  that this is _not_ the memory consumed by the document index, which
  [is loaded into unallocated system memory using mmap.](https://solr.apache.org/guide/8_9/jvm-settings.html#choosing-memory-heap-settings)
  You probably want to set this to around 25% of your Solr server's free RAM,
  though this is just a guideline.
- `SOLR_JAVA_MEM`: Finer-grained control than the above value; set minimum and
  maximum heap sizes with `"-Xms512m -Xmx512m"`.
- `SOLR_IP_WHITELIST`: Useful for increasing the security of your server by
  allowing it only to connect to a specified whitelist of IP addresses.
  Comma-separated list.

## Versioning

I update this image whenever a new version of Solr has been released. For image
version `x.y.z.w`, the Solr version bundled is `x.y.z` and `w` is the image
version.

## Maintenance

When you want to prepare a new release:

```console
$ sudo docker build -t pencechp/sciveyor-solr:latest .
$ sudo docker image tag pencechp/sciveyor-solr:latest pencechp/sciveyor-solr:8.x.y.z
$ sudo docker push pencechp/sciveyor-solr:latest
$ sudo docker push pencechp/sciveyor-solr:8.x.y.z
```

## License

As with [Sciveyor itself,](https://codeberg.org/sciveyor/sciveyor) this Docker
image is released under the MIT license.
